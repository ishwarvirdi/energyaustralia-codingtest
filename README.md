# Energy Australia Coding Test

## Tech Stack
- Front end = ReactJS
- Back end = Spring

## Steps to run/execute
- Go to the directory where you want to clone the repository. (Eg: /home/ABC/)
- Clone the repository using the command. ```git clone git@bitbucket.org:ishwarvirdi/energyaustralia-codingtest.git```
- Once the clone is complete, the Front end component is present in the `React` folder. Steps to run it is present in its `README.md` file.
- The Back end component is present in the `Spring` folder. Steps to run it is present in its `README.md` file.
