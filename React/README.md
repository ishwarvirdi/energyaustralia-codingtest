# Energy Australia Coding Test - Front end (ReactJS)

## Prerequisites
- Node.JS
- NPM

## Steps to run/execute
- After the repository has been cloned, go to React directory. (Eg: /home/ABC/energyaustralia-codingtest/React)
- Run `npm install` to install all packages/dependencies.
- Run `npm start` to run the Front end component locally.
- Go to `http://localhost:3000/` in the web browser.