import React, { Component } from 'react';
import './App.css';
import InputTable from './components/InputTable';
import OutputTable from './components/OutputTable';

import * as Constants from './config/constants';

import axios from 'axios';
import { Button } from 'reactstrap';
import { Alert } from 'reactstrap';
import Loader from 'react-loader-spinner';

// Sourced from https://www.kisspng.com/png-melbourne-energyaustralia-business-organization-bu-4439282/
const EALogo = require('./energyaustralia-logo.png');

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      data: {},
      error: {},
      loading: false
    };
    this.callAPI = this.callAPI.bind(this);
  }

  callAPI() {

    this.setState({
      loading: true,
      error: false,
    });

    axios({
      withCredentials: true,
      method: 'GET',
      url: Constants.backendURL,
    })
      .then(
        (response) => {
          if (response.data.error) {
            this.setState(response.data)
          } else {
            this.setState({
              data: response.data,
              error: false,
              loading: false
            })
          }
        }
      ).catch(
        (error) => {
          this.setState({
            data: {},
            error: true,
            code: 1001,
            message: "Error: Network Error",
            loading: false
          })
        })
  }


  render() {
    var noAPIData = (this.state.data === null || typeof this.state.data === 'undefined' || Object.keys(this.state.data).length === 0);
    var displayError = (this.state.error === true && typeof this.state.error !== 'undefined');
    return (
      <div className="App">
        <img src={EALogo} alt="Energy Australia Logo" />
        <h1>Coding Test - Cars API</h1>
        <hr />
        <Button outline color="success" onClick={this.callAPI}>
          {
            noAPIData ? "Get Data from Cars API" : "Refresh Tables"
          }
        </Button>
        <hr />
        {noAPIData || this.state.loading ? null : <InputTable headers={Constants.inputTableHeaders} payload={this.state.data.apiInput} />}
        <hr />
        {noAPIData || this.state.loading ? null : <OutputTable headers={Constants.outputTableHeaders} payload={this.state.data.apiOutput} />}
        <hr />
        {this.state.loading ? <Loader type="Oval" color="#000000" height={80} width={80} /> : null}
        {displayError ? <Alert color="danger">Error! Code: {this.state.code} / Message: {this.state.message}</Alert> : null}
        <hr />
      </div>
    );
  }
}

export default App;
