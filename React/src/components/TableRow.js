import React from 'react';

const TableRow = (inputs) => {

    return (
      <tr>
        <td>{inputs.a}</td>
        <td>{inputs.b}</td>
        <td>{inputs.c}</td>
      </tr>
    )
  
  }

export default TableRow;