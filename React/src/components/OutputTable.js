import React, { Component } from 'react';
import TableRow from './TableRow'
import { Table } from 'reactstrap';

class OutputTable extends Component {

  constructor(props) {
    super(props);
    this.renderTableData = this.renderTableData.bind(this);
    this.renderTableHeaders = this.renderTableHeaders.bind(this);
  }

  renderTableData() {
    var data = [];
    this.props.payload.forEach((eachMake) => {
      eachMake.models.forEach((eachModel) => {
        eachModel.showNames.forEach((eachShowName) => {
          data.push(
            <TableRow a={eachMake.makeName} b={eachModel.modelName} c={eachShowName} />
          );
        });
      });
    });

    return data;
  }

  renderTableHeaders() {
    return this.props.headers.map((eachHeader) => {
      return (
        <th>
          {eachHeader}
        </th>
      );
    });
  }

  render() {

    return (
      <div>
        <h3>Converted Output</h3>
        <div className="table-contents">
          <Table bordered>
            <thead>
              <tr>
                {this.renderTableHeaders()}
              </tr>
            </thead>
            <tbody>
              {this.renderTableData()}
            </tbody>
          </Table>
        </div>
      </div>
    );
  }
}

export default OutputTable;
