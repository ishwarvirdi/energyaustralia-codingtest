import React, { Component } from 'react';
import TableRow from './TableRow';

import { Table } from 'reactstrap';


class InputTable extends Component {

  constructor(props) {
    super(props);
    this.renderTableData = this.renderTableData.bind(this);
    this.renderTableHeaders = this.renderTableHeaders.bind(this);
  }

  renderTableData() {
    var data = [];
    this.props.payload.forEach((eachShow) => {
      eachShow.cars.forEach((eachCar) => {
        data.push(
          <TableRow a={eachShow.name} b={eachCar.make} c={eachCar.model} />
        );
      });
    }
    );
    return data;
  }

  renderTableHeaders() {
    return this.props.headers.map((eachHeader) => {
      return (
        <th>
          {eachHeader}
        </th>
      );
    });
  }

  render() {

    return (
      <div>
        <h3>Input Data from Cars API</h3>
        <div className="table-contents">
          <Table bordered>
            <thead>
              <tr>
                {this.renderTableHeaders()}
              </tr>
            </thead>
            <tbody>
              {this.renderTableData()}
            </tbody>
          </Table>
        </div>
      </div>
    );
  }
}

export default InputTable;
