package com.energyaustralia.codingtest;

import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.*;
import static org.springframework.test.web.client.response.MockRestResponseCreators.*;

import java.io.File;
import java.nio.file.Files;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.util.ResourceUtils;
import org.springframework.web.client.RestTemplate;

import com.energyaustralia.codingtest.controller.CarsController;
import com.energyaustralia.codingtest.service.ConverterService;
import com.fasterxml.jackson.databind.exc.MismatchedInputException;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CodingtestApplicationTests {

	@Autowired
	private CarsController carsController;

	@Autowired
	private ConverterService converterService;

	@Autowired
	private RestTemplate restTemplate;

	private MockRestServiceServer mockServer;

	@Value("${app.energyaustralia.carsapi.uri}")
	private String carsAPIURL;

	@Before
	public void setUp() {
		mockServer = MockRestServiceServer.bindTo(restTemplate).bufferContent().build();
	}

	@Test
	public void testWhenAPIReturnsNothing() throws Exception {

		mockServer.expect(requestTo(carsAPIURL)).andExpect(method(HttpMethod.GET))
				.andRespond(withStatus(HttpStatus.OK).body("\"\"").contentType(MediaType.TEXT_PLAIN));

		String response = carsController.getCarMakesDetail();

		mockServer.verify();

		assertThat(response,
				allOf(containsString("980"), containsString("Energy Australia's Cars API returned Empty response")));

	}

	@Test
	public void testGoodAPIData() throws Exception {

		File file = ResourceUtils.getFile("classpath:inputs/test1.txt");

		String content = new String(Files.readAllBytes(file.toPath()));

		ResponseEntity<String> mockResponse = new ResponseEntity<String>(content, HttpStatus.OK);

		String response = converterService.extractCarToShowsInfo(mockResponse);

		File correctOutputFile = ResourceUtils.getFile("classpath:correctOutputs/test1_APIOutput.txt");

		String correctOutput = new String(Files.readAllBytes(correctOutputFile.toPath()));

		assertThat(response, equalTo(correctOutput));

	}

	@Test
	public void testBadAPIData() throws Exception {

		File file = ResourceUtils.getFile("classpath:inputs/test2.txt");

		String content = new String(Files.readAllBytes(file.toPath()));

		ResponseEntity<String> mockResponse = new ResponseEntity<String>(content, HttpStatus.OK);

		try {
			String response = converterService.extractCarToShowsInfo(mockResponse);
			assertFalse(true);
		} catch (Exception e) {
			assertThat(e.getClass(), equalTo(MismatchedInputException.class));
		}
	}

}
