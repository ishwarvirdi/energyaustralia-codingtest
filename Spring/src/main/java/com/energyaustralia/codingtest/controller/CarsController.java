package com.energyaustralia.codingtest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.energyaustralia.codingtest.service.CarsProviderService;

/**
 * Controller for the exposed RESTful web services.
 * 
 * @author ivirdi
 *
 */
@RestController
public class CarsController {

	/**
	 * Service class which performs all the operations/tasks for the API calls.
	 */
	@Autowired
	private CarsProviderService carsProviderService;

	/**
	 * API to consumer the Coding Test Cars API and extract/produce the list of car
	 * models with shows attended in alphabetical order of the car's make.
	 * 
	 * @return API Output in JSON format.
	 */
	@RequestMapping(value = "carMakesDetail", method = RequestMethod.GET, produces = "application/json")
	public String getCarMakesDetail() {
		return carsProviderService.getCarMakesDetail();
	}

}
