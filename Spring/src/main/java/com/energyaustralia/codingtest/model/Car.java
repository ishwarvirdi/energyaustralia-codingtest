package com.energyaustralia.codingtest.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Model used by the Jackson JSON parser to convert the model element from the
 * Cars API into a Java object.
 * 
 * @author ivirdi
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Car {

	private String make = "";
	private String model = "";

	public String getMake() {
		return make;
	}

	public void setMake(String make) {
		this.make = make;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	/**
	 * Modified toString to format similar to a JSON one.
	 */
	@Override
	public String toString() {
		return "\"make\" : \"" + make + "\", \"model\" : \"" + model + "\"";
	}

}
