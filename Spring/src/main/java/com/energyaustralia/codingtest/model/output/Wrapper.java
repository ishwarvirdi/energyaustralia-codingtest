package com.energyaustralia.codingtest.model.output;

import java.util.List;

import com.energyaustralia.codingtest.model.CarShow;

/**
 * Wrapper class for the API's output. This will facilitate the formating of the
 * JSON response which is read/consumed by the front end app.
 * 
 * The API output when successful will contain the Response we get from the
 * Coding Test API (stored in apiInput) and this App's /carMakesDetail API
 * response (stored in apiOutput)
 * 
 * @author ivirdi
 *
 */
public class Wrapper {

	private List<CarShow> apiInput;

	private List<Make> apiOutput;

	public Wrapper(List<CarShow> apiInput, List<Make> apiOutput) {
		super();
		this.apiInput = apiInput;
		this.apiOutput = apiOutput;
	}

	public List<CarShow> getApiInput() {
		return apiInput;
	}

	public void setApiInput(List<CarShow> apiInput) {
		this.apiInput = apiInput;
	}

	public List<Make> getApiOutput() {
		return apiOutput;
	}

	public void setApiOutput(List<Make> apiOutput) {
		this.apiOutput = apiOutput;
	}

}
