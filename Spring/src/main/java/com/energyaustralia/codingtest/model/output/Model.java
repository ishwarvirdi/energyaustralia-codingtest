package com.energyaustralia.codingtest.model.output;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Model used by the Jackson JSON parser to convert the Java Model object
 * created by this app into JSON format for the API Output.
 * 
 * A Model has a name and a list/set of car show names.
 * 
 * @author ivirdi
 *
 */
public class Model {

	private String modelName;

	private Set<String> showNames;

	public Model(String modelName, String showName) {
		super();
		this.modelName = modelName;
		this.showNames = new HashSet<>(Arrays.asList(showName));
	}

	public String getModelName() {
		return modelName;
	}

	public void setModelName(String modelName) {
		this.modelName = modelName;
	}

	public Set<String> getShowNames() {
		return showNames;
	}

	public void setShowNames(Set<String> showNames) {
		this.showNames = showNames;
	}

}
