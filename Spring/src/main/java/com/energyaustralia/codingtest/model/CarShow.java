package com.energyaustralia.codingtest.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Model used by the Jackson JSON parser to convert the model element from the
 * Cars API into a Java object.
 * 
 * @author ivirdi
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CarShow {

	private String name = "";
	private List<Car> cars = new ArrayList<>();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Car> getCars() {
		return cars;
	}

	public void setCars(List<Car> cars) {
		this.cars = cars;
	}

	/**
	 * Modified toString to format similar to a JSON one.
	 */
	@Override
	public String toString() {
		return "\"name\" : \"" + name + "\", \"cars\" : " + cars;
	}

}
