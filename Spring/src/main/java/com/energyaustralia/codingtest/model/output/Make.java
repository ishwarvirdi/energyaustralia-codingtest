package com.energyaustralia.codingtest.model.output;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Model used by the Jackson JSON parser to convert the Java Make object created
 * by this app into JSON format for the API Output.
 * 
 * A Make has a name and a list of Models.
 * 
 * @author ivirdi
 *
 */
public class Make implements Comparable<Make> {

	private String makeName;

	private List<Model> models;

	public Make(String makeName, String modelName, String showName) {
		super();
		this.makeName = makeName;
		Model newModel = new Model(modelName, showName);
		this.models = new ArrayList<>(Arrays.asList(newModel));
	}

	public String getMakeName() {
		return makeName;
	}

	public void setMakeName(String makeName) {
		this.makeName = makeName;
	}

	public List<Model> getModels() {
		return models;
	}

	public void setModels(List<Model> models) {
		this.models = models;
	}

	/**
	 * To facilitate the sorting of List of Makes in alphabetical order.
	 *
	 */
	@Override
	public int compareTo(Make o) {
		return this.makeName.compareTo(o.getMakeName());
	}

}
