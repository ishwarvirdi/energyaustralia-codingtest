package com.energyaustralia.codingtest.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.energyaustralia.codingtest.exception.CarsException;

/**
 * Service class which will perform the calling to the Coding Test API, handling
 * of incorrect response and mapping from JSON elements to Java objects. This
 * Service will pass on to the ConverterService to perform the
 * manipulations/conversions for the API output.
 * 
 * @author ivirdi
 *
 */
@Service
public class CarsProviderService {

	@Value("${app.energyaustralia.carsapi.uri}")
	private String carsAPIURL;

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private ConverterService converterService;

	/**
	 * Entry point to start working on the /carMakesDetail API response. We extract
	 * the response from the Coding Test API and create the required response.
	 * 
	 * @return carMakesDetail API response or exception message for front end app's
	 *         consumption (if encountered)
	 */
	public String getCarMakesDetail() {
		try {
			ResponseEntity<String> apiResponse = restTemplate.getForEntity(carsAPIURL, String.class);
			if (checkAPIResponse(apiResponse)) {
				return converterService.extractCarToShowsInfo(apiResponse);
			} else {
				return new CarsException(1050, "Error in Energy Australia's Cars API Call").toString();
			}

		} catch (CarsException e) {
			return e.toString();
		} catch (Exception e) {
			// Wrapping it into a front end readable format.
			return new CarsException(999, e.getMessage()).toString();
		}

	}

	/**
	 * Checking and handling the responses of the Coding Test API. Found to be a bit
	 * unstable.
	 * 
	 * @param apiResponse will contain the HTTP status code and Body (for the App's
	 *                    consumption later)
	 * @return True if the API response is good to process.
	 * @throws The generic/wrapper exception class CarsException
	 * @throws Any other Exception
	 */
	private boolean checkAPIResponse(ResponseEntity<String> apiResponse) throws CarsException, Exception {
		if (apiResponse.getStatusCode() == HttpStatus.OK) {
			String apiResponseBody = apiResponse.getBody();

			if (apiResponseBody.equals("\"\"")) {

				throw new CarsException(980, "Energy Australia's Cars API returned Empty response");
			}

			return true;

		} else
			throw new CarsException(985, apiResponse.getStatusCode().toString());
	}

}
