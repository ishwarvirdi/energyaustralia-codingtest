package com.energyaustralia.codingtest.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.energyaustralia.codingtest.model.Car;
import com.energyaustralia.codingtest.model.CarShow;
import com.energyaustralia.codingtest.model.output.Make;
import com.energyaustralia.codingtest.model.output.Model;
import com.energyaustralia.codingtest.model.output.Wrapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Service class which will perform the conversion of data received from the
 * Coding Test API to the /carMakesDetail required format (Makes >> Models >>
 * Shows).
 * 
 * @author ivirdi
 *
 */
@Service
public class ConverterService {

	ObjectMapper objectMapper = new ObjectMapper();

	/**
	 * This method first converts the Coding Test API from JSON to Java objects and
	 * then performs the conversion from (Shows >> Make-Model(s)) to the (Makes >>
	 * Models >> Shows) structure.
	 * 
	 * @param allCarShows contains all the entries/elements from Coding Test API
	 *                    response
	 * @return Response to the /carMakesDetail API
	 * @throws JsonProcessingException if any issue occurs when creating the JSON
	 *                                 response string
	 */
	public String extractCarToShowsInfo(ResponseEntity<String> apiResponse) throws Exception {

		String apiResponseBody = apiResponse.getBody();

		List<CarShow> allCarShows = objectMapper.readValue(apiResponseBody, new TypeReference<List<CarShow>>() {
		});

		List<Make> allMakes = new ArrayList<>();

		for (CarShow eachCarShow : allCarShows) {

			// Ignoring elements with no car show name
			if (eachCarShow.getName().isEmpty())
				continue;

			for (Car eachCar : eachCarShow.getCars()) {

				// Ignoring elements with no make or model name
				if (eachCar.getMake().isEmpty() || eachCar.getModel().isEmpty())
					continue;

				int i = getIndexOfMake(allMakes, eachCar.getMake());
				if (i != -1) {
					int j = getIndexOfModel(allMakes.get(i).getModels(), eachCar.getModel());
					if (j != -1) {
						allMakes.get(i).getModels().get(j).getShowNames().add(eachCarShow.getName());
					} else {
						allMakes.get(i).getModels().add(new Model(eachCar.getModel(), eachCarShow.getName()));
					}
				} else {
					allMakes.add(new Make(eachCar.getMake(), eachCar.getModel(), eachCarShow.getName()));
				}
			}
		}

		// Sort the list of makes in alphabetical order
		Collections.sort(allMakes);

		// Wrapping/creating the /carMakesDetail API response
		Wrapper apiOutput = new Wrapper(allCarShows, allMakes);

		return convertToJson(apiOutput);
	}

	/**
	 * Method to convert the API response (Java Object) into a String in JSON
	 * format.
	 * 
	 * @param apiOutput
	 * @return
	 * @throws JsonProcessingException
	 */
	private String convertToJson(Wrapper apiOutput) throws JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
		return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(apiOutput);
	}

	/*
	 * Below methods are used instead since List.getIndexOf(String) was not working
	 * properly (has its limitations).
	 */

	private int getIndexOfMake(List<Make> allMakes, String make) {

		int i = 0;
		for (Make eachMake : allMakes) {
			if (eachMake.getMakeName().equals(make))
				return i;
			++i;
		}

		return -1;
	}

	private int getIndexOfModel(List<Model> allModels, String model) {
		int i = 0;
		for (Model eachModel : allModels) {
			if (eachModel.getModelName().equals(model))
				return i;
			++i;
		}
		return -1;
	}

}
