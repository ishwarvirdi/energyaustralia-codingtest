package com.energyaustralia.codingtest.exception;

/**
 * Generic exception class containing the Error code and message which will be
 * consumed/displayed by the front end.
 * 
 * @author ivirdi
 *
 */
public class CarsException extends Exception {

	private static final long serialVersionUID = 1L;

	private int code;
	private String message;

	public CarsException(int code, String message) {
		super();
		this.code = code;
		this.message = message;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * The exception is being formated so as to be recognized by the front end app.
	 */
	@Override
	public String toString() {
		return "{ \"data\" : {}, \"loading\" : false , \"error\" : true, \"code\" : " + code + ", \"message\" : \""
				+ message + "\" }";
	}

}
