package com.energyaustralia.codingtest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * App to consume the Energy Australia Coding Test Cars API. This app converts
 * the API data to produce a list of models of car makes along with the show
 * names they attended. The list has be grouped and ordered alphabetically of
 * the car's make.
 * 
 * The list can be accessible using the created/exposed API "/carMakesDetail" of
 * this app.
 * 
 * @author ivirdi
 *
 */
@ComponentScan("com.energyaustralia.codingtest")
@SpringBootApplication
public class CodingtestApplication {

	public static void main(String[] args) {
		SpringApplication.run(CodingtestApplication.class, args);
	}

}
