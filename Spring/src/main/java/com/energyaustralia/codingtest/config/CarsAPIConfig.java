package com.energyaustralia.codingtest.config;

import org.hobsoft.spring.resttemplatelogger.LoggingCustomizer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.filter.CorsFilter;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

/**
 * Configuration Class to declare the Beans to be used by the Spring App.
 * 
 * @author ivirdi
 *
 */
@Configuration
public class CarsAPIConfig {

	@Value("${frontend.endpoint}")
	private String frontendURL;

	/**
	 * RestTemplate to access/consume the Cars API. The Template is added with a
	 * customizer to improve Debug logging. Enabled with the configuration
	 * logging.level.org.hobsoft.spring.resttemplatelogger.LoggingCustomizer.
	 * 
	 * @return RestTemplate object with improved Debug logging
	 */
	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplateBuilder().customizers(new LoggingCustomizer()).build();
	}

	/**
	 * To enable Cross-Origin Resource Sharing (CORS) with the ReactJS front end
	 * app.
	 * 
	 * @return
	 */
	@Bean
	public CorsFilter corsFilter() {
		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		CorsConfiguration config = new CorsConfiguration();
		config.setAllowCredentials(true);
		config.addAllowedOrigin(frontendURL);
		config.addAllowedHeader("*");
		config.addAllowedMethod("OPTIONS");
		config.addAllowedMethod("GET");
		config.addAllowedMethod("POST");
		config.addAllowedMethod("PUT");
		config.addAllowedMethod("DELETE");
		source.registerCorsConfiguration("/**", config);
		return new CorsFilter(source);
	}

}
