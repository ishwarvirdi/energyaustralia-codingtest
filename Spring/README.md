# Energy Australia Coding Test - Back end (Spring)

## Prerequisites
- Java
- Maven

## Steps to run/execute
- After the repository has been cloned, go to Spring directory. (Eg: /home/ABC/energyaustralia-codingtest/Spring)
- Run `mvn clean package` to install all packages/dependencies and create the deployable JAR.
- Go to the target folder.
- Run `java -jar codingtest-0.0.1-SNAPSHOT.jar` to run the Spring app and exposing the API.

## To Test API call without React Front end
- Run `curl -X GET "http://localhost:8080/carMakesDetail"` using Terminal.
- or
- Open `http://localhost:8080/carMakesDetail` in the web browser.
